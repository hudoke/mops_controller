from time import sleep
from datetime import datetime
import gphoto2 as gp
import signal, os, subprocess
import sys
# If debugging not on RASPI comment te next line 
import RPi.GPIO as GPIO
import logging
import socket
import time
import signal, os, subprocess 
import Settings

# Global variable - DONT CHANGE STUFF HERE!!
cameras_l = []
currentSession = 0
numberOfSessionPictures = 0
#Standard Directory is this:
safeDIR = "Session1/"
pictureFormat = Settings.PICTURE_FORMAT
getImageTimeout = Settings.TIMEOUT

# Logging Variables
now = datetime.now().strftime("%H:%M:%S")
logname = "mopsController_" + now + ".log"
logger = logging.getLogger()

# Parameters - CHANGE IN Settings.py NOT HERE!! 
massStorageDevice = Settings.MASS_STORAGE
pinumber = Settings.PI_NUMBER



# Function to send a message to the Client
def sendMsg(msg):
    ledClient.send(bytes(msg,"utf-8"))

# Function that checks if a message was sent by the client
def checkforMsg():
    try:
        msg = clientsocket.recv(8, 0x40)
        msgdec = msg.decode("utf-8")
        if msgdec != "":
            logging.info("Controller received message:" + msgdec)
        return msgdec
    except:
        a = 1+1


#In later versions pi should be substituted by getting the current username


def killgphoto2Process():
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    # Find processes that should be killed
    for line in out.splitlines():
        if b'gvfsd-gphoto2' in line:
            #KILL KILL KILL
            pid = int(line.split(None,1)[0])
            logmessage = "Just killed process: " + str(pid) + "\n"
            logging.info(logmessage)
            os.kill(pid, signal.SIGKILL)

def setupTransfer():
    # Check if SSD is connected by requesting the path
    # if not exit program
    if not os.path.exists(massStorageDevice):
        logmessage = "ERROR!! - NO MASS STORAGE DEVICE DETECTED"
        logging.info(logmessage)
        changeLEDBehaviour(1)
        exit(1)

    if os.path.exists(massStorageDevice[:-1] + '1/'):
        logmessage = "WARNING!! - Storage Device was not removed correctly - trying to recover"
        logging.info(logmessage)
        p =  subprocess.Popen(['sudo','umount','/dev/sda1'],stdout=subprocess.PIPE)
        out, err = p.communicate()
        p1 = subprocess.Popen(['sudo','rm','-rf','/media/pi/Intenso'], stdout=subprocess.PIPE)
        out1, err1 = p1.communicate()
        p2 = subprocess.Popen(['udisksctl','mount','-b','/dev/sda1'],stdout=subprocess.PIPE)
        out2, err2 = p2.communicate()
        logmessage = 'Recovery completed with: \n' + str(out2)
        logging.info(logmessage)

    # List all files and directories on the external drive
    dirList = os.listdir(massStorageDevice)

    # Iterate through all files and directory and only keep the numbers that match our naming 
    # 'Session' needs to be changed if the naming of directory is changed
    dirIndex = 0
    path = ""
    for x in dirList:
        if 'Session' in x:
            if int(x.replace('Session','')) > dirIndex:
                dirIndex = int(x.replace('Session',''))
    if dirIndex >0:
        path = massStorageDevice + safeDIR.replace('1',str(dirIndex+1))
    else:
        path = massStorageDevice + safeDIR
    if(path == ""):
        logging.info("Error!!! - path is still NULL")
        exit(1)

    # Create Session folder if not already present
    if not os.path.exists(path):
        os.makedirs(path)
    
    # Change log directory to move logfile on the USB device
    logname = path + "logfile.log"
    logger.handlers[0].stream.close()
    logger.removeHandler(logger.handlers[0])
    file_handler = logging.FileHandler(logname)
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s %(filename)s, %(lineno)d, %(funcName)s: %(message)s")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    
    #Discover how many pictures were already taken this session
    maxIndex = 0
    pictureList = os.listdir(path)
    if len(pictureList):
        for x in pictureList:
            # Ignore all items that do not contain 'camera'
            # Need to be changed if naming of files changes
            if "camera" in x:
                xIndex = int(x[-3:])
                if xIndex > maxIndex:
                    maxIndex = xIndex

    
    # Set data if obtained and move working directory to the current session
    numberOfSessionPictures = maxIndex+1
    os.chdir(path)
    logmessage = ("Done with setup: \n Session and next Index are: " + path +" and " + str(numberOfSessionPictures) ) 
    logging.info(logmessage)

def SetupCameras():
    #Detect how many cameras are connected
    #by iterating over gphoto2's autodetect
    p = subprocess.Popen(['gphoto2', '--auto-detect'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.splitlines():
        if b'Canon EOS 1500D' in line:
            #Add all cameraports to list
            usbPortID = (str(line).split("usb:")[1])
            usbPortID = usbPortID.split(None,1)[0]
            cameras_l.append("usb:"+usbPortID)

    # Throw error exit if not all cameras are detected
    if not len(cameras_l) >= Settings.NUMBER_OF_CAMERAS:
        logmessage = "ERROR!! - NOT ENOUGH CAMERAS DETECTED!! DETECTED: " + str(len(cameras_l))
        logging.info(logmessage)
        changeLEDBehaviour(1)
        exit(1)
        
    logmessage = ("Done with Camera setup: \n Cameras detected: " + str(cameras_l) ) 
    logging.info(logmessage)


def GetImages():

    if not os.path.exists(massStorageDevice):
        logmessage = "ERROR!! - MASS STORAGE DEVICE REMOVED"
        changeLEDBehaviour(2)
        logging.info(logmessage)
        exit(1)

    changeLEDBehaviour(4)

    # For each camera that was detected at the start a subprocess is started to request data
    for camera in cameras_l:

        # Set program args according to gphoto2 syntax
        thisArgs = ("--port="+camera)

        # File naming is set here - changes here need to be accorded for in filenumber detection
        filename = "--filename="+"pi-"+str(pinumber)+"_camera-"+ camera.split(',',1)[1] +"_%Y-%m-%"+"d_%H_%03n" + pictureFormat

        # In later versions numberOfSessionPictures could be reworked to track image numbering per camera
        global numberOfSessionPictures
        global getImageTimeout
        filenumber = "--filenumber="+ str(numberOfSessionPictures)
        try:
            # Start process to get data from camera - with a max time of 30 seconde
            p = subprocess.Popen(["gphoto2", "--get-all-files", "--new", "-D", "-R",thisArgs,filename,filenumber], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            # Timeoutleads to issues, so it will not be used, TODO: rework to get images in sort bursts - out,err = p.communicate(timeout=getImageTimeout)
            out,err = p.communicate()
            lines = 0

            logmessage = ('err: ' + str(err) +" output: " + str(out.decode()))
            logging.info(logmessage)
            if str(err) != "b''":
                if "timed out after" not in str(err):
                    changeLEDBehaviour(2)
                    logging.info("Shuting down because: " + str(err))
                    exit(1)
                logging.info("Timed out after 30s with output" + str(out))


            # If images were pulled, increase pictureIndex
            if out != None:
                lines = out.decode().count("\n")
                numberOfSessionPictures=numberOfSessionPictures+lines
                logging.info("Done with camera: "+ str(camera) + ", downloaded " + str(lines))

        # If any error occurs while executing the gphoto2 command they can be received here
        # In future version the error handling in here should be extended
        except (subprocess.CalledProcessError, subprocess.TimeoutExpired) as e:

            stderror = e.stderr
            stdout = e.output
            if "timed out after" not in str(stdout) and (str(stderror) != "None"):
                changeLEDBehaviour(2)
                logging.info("Exception caught: "+ str(stderror))
            logging.info("Timed out after 30s with output: " + str(stdout))
            p.kill()

            
    sleep(0.1)
    changeLEDBehaviour(0)



def changeLEDBehaviour(code):
    #set voltages according to color
    match code:
        case 0:
            # All good - green
            sendMsg("normal")
        case 1:
            # Error in setup - orange
            sendMsg("setuperror")
        case 2:
            # Runtimeerror  - red
            sendMsg("error")
        case 3:
            # Setup in progress - white
            sendMsg("setup")
        case 4:
            # Writing to Disk - blue
            sendMsg("reading")
    



## CONTROL LOOP
##
# Instantiate Server Socket for inter process communication
# This has to be done first to ensure the debug LEDs work!
sok = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sok.bind((socket.gethostname(), 12345))
sok.listen(5)

# Wait for a successful connection with the server
# TODO: Is this loop correct?
connected = False
while not connected:
    clientsocket, address = sok.accept()
    clientsocket.send(bytes("Welcome to the server","utf-8"))
    ledClient = clientsocket
    connected = True
sleep(10)

#Start Logging in /home on the pi
logger.setLevel(logging.DEBUG)
file_handler = logging.FileHandler(logname)
file_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s %(filename)s, %(lineno)d, %(funcName)s: %(message)s")
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


setupTransfer()
SetupCameras()







# As a gphoto2 process will be started if a camera is detected 
# it needs to be killed first, as it blocks accesss to the camera
killgphoto2Process()

# Run setup routine to aquire the current Session folder and configure GPIO
GPIO.setmode(GPIO.BCM)
#GPIO.setup(gpioPinForButton, GPIO.IN, pull_up_down=GPIO.PUD_UP)


# If setup was successfull change LED to signify it
changeLEDBehaviour(0)
sleep(1)
# Testcode that runs routine 5 times
done = False
while not done:
    GetImages()
    sleep(2)

