# LED strip configuration:
LED_COUNT = 3  # Number of LED pixels.
LED_PIN = 18            # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10          # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000    # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10            # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255    # Set to 0 for darkest and 255 for brightest
LED_INVERT = False      # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0         # set to '1' for GPIOs 13, 19, 41, 45 or 53
MASS_STORAGE = "/media/pi/Intenso/" #Set to name of connected mass storage device
PI_NUMBER = 1           # To keep filenames unique, set a unique umber/ID for each raspi in the system
NUMBER_OF_CAMERAS = 1   # Enter the number of cameras that should be detected on startup
PICTURE_FORMAT = ".jpg" # Enter the format the images should be saved as on the external Storage, should be the same as the cameras output format
TIMEOUT = 10            # Timeout after which the get all images gphoto2 command should be finished  