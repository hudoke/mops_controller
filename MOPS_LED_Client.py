import socket
import time
from rpi_ws281x import PixelStrip, Color
import argparse
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import Settings

# LED strip configuration:
LED_COUNT = Settings.LED_COUNT          # Number of LED pixels.
LED_PIN = Settings.LED_PIN              # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10                          # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = Settings.LED_FREQ_HZ      # LED signal frequency in hertz (usually 800khz)
LED_DMA = Settings.LED_DMA              # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = Settings.LED_BRIGHTNESS    # Set to 0 for darkest and 255 for brightest
LED_INVERT = Settings.LED_INVERT            # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = Settings.LED_CHANNEL          # set to '1' for GPIOs 13, 19, 41, 45 or 53




# Setup Client Socket
sok = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sok.connect((socket.gethostname(), 12345))


# Callback function for GPIO events
# Checks if a message should be sent to the Server

# Initialize GPIO for Buttons
GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering


# Wait for a successfull connection with the server
connection = False
while not connection:
    msg = sok.recv(1024)
    msgdec = msg.decode("utf-8")
    if(msgdec=="Welcome to the server"):
        connection = True
        print("Connection established")



# Define functions which animate LEDs in various ways.


# Modify the brightness based on the modifier

    


# Check it the server has sent a message and decode it
def checkforMsg():
    try:
        msg = sok.recv(1024, 0x40)
        msgdec = msg.decode("utf-8")
        if msgdec != "":
            print("received:" + msgdec)
        return msgdec
    except:
        a = 1+1


# Create NeoPixel object with appropriate configuration.
strip = PixelStrip(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
# Initialize the library (must be called once before other functions).
strip.begin()
for i in range(strip.numPixels()):
    strip.setPixelColor(i, Color(255,255,255))


# Main control loop


done = False
while not done:
    
    msg = checkforMsg()
    
    if msg == "stop":
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, Color(0,0,0))
    elif msg == "setup":
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, Color(255,255,255))
    elif msg == "normal":
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, Color(0,255,0))
    elif msg == "error":
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, Color(255,0,0))
    elif msg == "setuperror":
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, Color(255,128,0))
    elif msg == "reading":
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, Color(0,0,255))
    strip.show()
        
   
    time.sleep(20 / 1000.0)
