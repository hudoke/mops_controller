# MOPS_Controller



## M.O.P.S. - Mobiles Oberflächen Photogrammetrie System
WICHTIG!
Das update.sh Shellcript löscht alle lokal erstellten Änderungen um Merge Konflikte zu vermeiden. Sind lokale wichtige Änderungen vorhanden, müssen Updates manuell ausgeführt werden.



Das mobile Oberflächen Photogrammetrie System oder kurz M.O.P.S. soll 8 Canon 2000D Kameras nutzen um akkurate 3D Modelle von verschiedensten Oberflächen
zu erstellen. Dazu werden je 4 Kameras von einem Raspberry Pi angesteuert der für die Bild Extraktion, sowie Status LEDs zuständig ist. Im späteren 
Verlauf könnten die Raspberry Pis auch verwendet werden um das System komplett zu automatisieren, die momentane Hauptaufgabe ist jedoch die Foto Extraktion.
Dazu sollen die beiden Raspberry Pis alle geschossenen Bilder von je 4 Kameras auf eine externe SSD schreiben und den Prozessstatus über farbige LEDs anzeigen.
Die Anforderungen können in folgende Domänen unterteilt werden:
* Input über Schalter an GPIO Pins abfragen
* Wenn Schalter gedrückt wird neue Session (Ordner) auf externem Laufwerk erstellen und als neues Ziel für eingehende Bilder festlegen
* Automatisch beim Start erkennen ob externe Laufwerke und Kameras angeschlossen sind
* Skript das kontinuierlich neue Bilder von allen angeschlossenen Kameras auf das externe Laufwerk verschiebt
* Programmstatus/Fehler über LEDs anzeigen

Deren genauere Funktion wird direkt im Code und hier im nachfolgenden aufgeschlüsselt.

TODO: Doku
